package lt.ekgame.compiler.test;import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.Parser;

public class Tester {

	public static void main(String[] args) throws FileNotFoundException 
	{
		Parser parser = new Parser(new FileReader(new File("code.txt")));
		Context context = new Context(parser);
		context.parse();
		context.process();
		List<String> code = context.compile();
		int i = 0;
		for (String str : code)
		{
			System.out.println(String.format("%-3d => \"%s\",", i, str));
			i++;
		}
	}

}
