package lt.ekgame.compiler.test;

public class DivTest {

	public static void main(String[] args) {
		
		int R = 0;
		int Q = 71;
		int divisor = 2;
		
		for (int i = 0; i < 8; i++)
		{
			print(R, Q, "");
			R <<= 1; R = R & 0xFF;
			if ((Q & 0x80) > 0)
				R++;
			Q <<= 1; Q = Q & 0xFF;
			
			print(R, Q, " shift");
			
			R -= divisor;
			print(R, Q, " subtract");
			
			if (R >= 0)
			{
				Q = Q | 1;
				print(R, Q, " set Q low to 1");
			}
			else
			{
				R += divisor;
				print(R, Q, " restore");
			}
			
		}
		
		System.out.println(Q);
	}
	
	private static void print(int R, int Q, String desc)
	{
		String strR = Integer.toBinaryString(R);
		if (strR.length() > 8) strR = strR.substring(strR.length() - 8, strR.length());
		System.out.println(String.format("%8s %8s %s", strR, Integer.toBinaryString(Q), desc));
	}

}
