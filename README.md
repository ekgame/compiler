# Running #

To run the project, clone the repository as a Java project in Eclipse or otherwise find a way to compile the code. Currently, the only way to compile the machine-code is to run the lt.ekgame.compiler.test.Tester class. By default it compiles the code.txt file and outputs copy/paste ready code to the console.

## Basic syntax ##

### Labels ###
```
#!none
label: *code*
```
Labels are used to jump around the code. Conditional statements require labels to jump to a different place in program when a condition is met.
Even if the code only finds a label in a an otherwise empty line, then it is still compiled as a line filled with zeros.

### MUX selection and writing to registers ###
MUX selection is done by simply writing down the register you want to select:
```
#!none
in;          # selects the input in the MUX
```
The valid arguments are:

* **in** - the input
* Letters **A** thruogh **F** - the registers

To write data from one register to another, you can use the assigner:
```
#!none
in -> B, C;  # selects the input in the MUX and writes it in to the B and C registers
```
There may be multiple registers after the assigner who will all be marked for writing at the same time.

The assigner may be used alone with some registers to mark the for writing without selecting anything in MUX.
```
#!none
-> A;       # effectively forces A register to save it's value
```

### Conditions ###
Conditions will force the processor jump from one place to another if one of the possible conditions is met. The places to jump to must be defined with labels.
```
#!none
# If the counter has finished, jump to the line with  label "finish"
if CNT -> finish; 

# If the counter has finished, jump to the line with label "finish". 
# If not - jump to "continue".
if CNT -> finish : continue;  
```
The possible conditions are:

* **x_H** where *x* is a letter from **A** to **F** - check if the last bit (left-most) in the register is *1*
* **x_L** where *x* is a letter from **A** to **F** - check if the first bit (right-most) in the register is *1*
* **CNT** - check if the counter has finished
* **ALU_H** - check if the last bit in the ALU result is 1
* **JUMP** - always true, will jump to the given label.

### ALU ###
ALU function can do some basic arithmetic functions. Only one function may be declared per line. The function may only have one argument.
```
#!none
ALU(ADD);  # Adds the contents of A register and the current MUX output.
```
The arguments are:
* **ADD** - add up the A register and current MUX output
* **NOT_L** - invert the MUX output
* **NOT_R** - invert the A register
* **L_ADD** - add one to the MUX output
* **L_SUB** - subtract one from the MUX output
* **R_ADD** - add one to the A register
* **R_SUB** - subtract one from the A register
* **XOR** - apply xor operation between the MUX output and the A register

### Reset ###
This function is used to reset registers to their default value.
```
RESET(A, B, CNT); # reset A, B registers and the counter
```

Possible arguments:

* Letters **A** through **F**, describing the registers. They will reset to zero
* **CNT** - resets the counter back to 8
* **ROM** - resets the ROM to it's default state
* **FLAG** - resets the flag register

** Counter **
The counter function decreases counter value by one every time it is used. You may use ```RESET(CNT);``` to reset counter back to 8.
```
CNT();   # decrease counter by one
``` 

### Shifting ###
Shifting functions allow to shift registers as needed:
```
LL(A, B);   # shift A and B registers to left (logic shift)

CL(C, D);   # shift C and D registers to left (cycle shift)

AR(E, F);   # shift E and F registers to right (arithmetic shift)
```

Possible functions:

* **LL** or **LR** - logic shift to left or right
* **CL** or **CR** - cycle shift to left or right
* **AL** or **AR** - arithmetic shift to left or right

### Return ###
Return function outputs the MUX output to the main machine output, effectively halting the whole execution.
```
B; OUT(B);  # select B register and mark it for output
RETURN();   # output the selected register
```

### Out ###
OUT function selects a register for output, as demonstrated in the description of the ```RETURN``` function above.

### Empty lines ###
Sometimes you will need to generate some empty lines to let the processor update the flag register or the counter. In that scenario, you may want to use the ```EMPTY``` function.
```
CNT();   # decrese the counter by one
EMPTY(); # empty line to let the counter update
if CNT -> finish : try_again; # perform the check
```

### Comments ###
The code may contain Python style whole line comments. Both comment-only and empty lines are not compiled. If you need an empty line you may use the ```EMPTY()``` functions.