package lt.ekgame.compiler.functions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.LineData;
import lt.ekgame.compiler.ParsingException;

public class FunctionALU extends Function {
	
	public static final Map<String, Integer> OFFSETS = new HashMap<>();
	public static final int OFFSET = 56;
	
	static {
		OFFSETS.put("ADD",   OFFSET);
		OFFSETS.put("NOT_L", OFFSET + 1);
		OFFSETS.put("NOT_R", OFFSET + 2);
		OFFSETS.put("L_ADD", OFFSET + 3);
		OFFSETS.put("L_SUB", OFFSET + 4);
		OFFSETS.put("R_ADD", OFFSET + 5);
		OFFSETS.put("R_SUB", OFFSET + 6);
		OFFSETS.put("XOR",   OFFSET + 7);
	}
	
	@Override
	public void execute(Context context, LineData line, List<String> arguments) 
	{
		if (arguments.size() == 0)
			throw new ParsingException("Failed to parse ALU function - no arguments");
		if (arguments.size() > 1)
			throw new ParsingException("Failed to parse ALU function - too many arguments");
		
		String arg = arguments.get(0);
		if (!OFFSETS.containsKey(arg))
			throw new ParsingException("Unknown alu function \"" + arg + "\"");
		
		line.setDataAt(OFFSETS.get(arg), true);
	}
}
