package lt.ekgame.compiler.functions;

import java.util.List;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.LineData;
import lt.ekgame.compiler.ParsingException;

public class FunctionOUT extends Function {

	@Override
	public void execute(Context context, LineData line, List<String> arguments) 
	{
		for (String arg : arguments)
		{
			if (!LineData.REGISTER_OFFSETS.containsKey(arg))
				throw new ParsingException("Failed to parse OUT function - unknown argument \"" + arg + "\"");
			line.setDataAt(LineData.REGISTER_OFFSETS.get(arg), true);
		}
	}

}
