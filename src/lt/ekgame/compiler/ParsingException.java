package lt.ekgame.compiler;

public class ParsingException extends RuntimeException {
	
	private static final long serialVersionUID = 3133624074247140019L;
	
	private int characterIndex = -1;

	public ParsingException(String string) {
		super(string);
	}

	public ParsingException(String string, int characterIndex) {
		super(string);
		this.characterIndex = characterIndex;
	}
	
	public int getCharacterIndex() {
		return characterIndex;
	}
}
