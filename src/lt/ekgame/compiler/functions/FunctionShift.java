package lt.ekgame.compiler.functions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.LineData;
import lt.ekgame.compiler.ParsingException;

public class FunctionShift extends Function {
	
	public enum Shift {
		LOGIC_LEFT("LL"), LOGIC_RIGHT("LR"), 
		ARITHMETIC_LEFT("AL"), ARITHMETIC_RIGHT("AR"), 
		CYCLE_LEFT("CL"), CYCLE_RIGHT("CR");
		
		public String name;
		Shift(String name) {this.name = name;}
	}
	
	public static final Map<Shift, Integer> OFFSETS = new HashMap<>();
	
	static {
		OFFSETS.put(Shift.LOGIC_LEFT,       2);
		OFFSETS.put(Shift.LOGIC_RIGHT,      3);
		OFFSETS.put(Shift.ARITHMETIC_LEFT,  4);
		OFFSETS.put(Shift.ARITHMETIC_RIGHT, 5);
		OFFSETS.put(Shift.CYCLE_LEFT,       6);
		OFFSETS.put(Shift.CYCLE_RIGHT,      7);
	}
	
	private Shift shiftType;
	
	public FunctionShift(Shift shiftType)
	{
		this.shiftType = shiftType;
	}

	@Override
	public void execute(Context context, LineData line, List<String> arguments) 
	{
		for (String arg : arguments)
		{
			if (!LineData.REGISTER_OFFSETS.containsKey(arg))
				throw new ParsingException("Failed to parse " + shiftType.name + " function - unknown argument \"" + arg + "\"");
			line.setDataAt(LineData.REGISTER_OFFSETS.get(arg) + OFFSETS.get(shiftType), true);
		}
	}

	@Override
	public boolean isCompatible(Function other)
	{
		if (!(other instanceof FunctionShift))
			return true;
		FunctionShift o = (FunctionShift) other;
		return o.shiftType != this.shiftType;
	}
	
	

}
