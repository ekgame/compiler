package lt.ekgame.compiler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lt.ekgame.compiler.functions.Function;
import lt.ekgame.compiler.functions.FunctionALU;
import lt.ekgame.compiler.functions.FunctionCNT;
import lt.ekgame.compiler.functions.FunctionEmpty;
import lt.ekgame.compiler.functions.FunctionOUT;
import lt.ekgame.compiler.functions.FunctionReturn;
import lt.ekgame.compiler.functions.FunctionReset;
import lt.ekgame.compiler.functions.FunctionShift;

public class Context {
	
	private List<LineData> data = new ArrayList<>();
	private static Map<String, Function> functionMap = new HashMap<>();
	private int generatedJumps = 0;
	
	static {
		functionMap.put("EMPTY",  new FunctionEmpty());
		functionMap.put("ALU",    new FunctionALU());
		functionMap.put("OUT",    new FunctionOUT());
		functionMap.put("CNT",    new FunctionCNT());
		functionMap.put("RESET",  new FunctionReset());
		functionMap.put("RETURN", new FunctionReturn());
		functionMap.put("LL",     new FunctionShift(FunctionShift.Shift.LOGIC_LEFT));
		functionMap.put("LR",     new FunctionShift(FunctionShift.Shift.LOGIC_RIGHT));
		functionMap.put("AL",     new FunctionShift(FunctionShift.Shift.ARITHMETIC_LEFT));
		functionMap.put("AR",     new FunctionShift(FunctionShift.Shift.ARITHMETIC_RIGHT));
		functionMap.put("CL",     new FunctionShift(FunctionShift.Shift.CYCLE_LEFT));
		functionMap.put("CR",     new FunctionShift(FunctionShift.Shift.CYCLE_RIGHT));
	}
	
	private Parser parser;
	
	public Context(Parser parser)
	{
		this.parser = parser;
	}
	
	public Function getFunction(String name) {
		if (!functionMap.containsKey(name))
			throw new ParsingException("Unknown function " + name);
		return functionMap.get(name.toUpperCase());
	}
	
	public void addData(LineData data) {
		if (data != null)
			this.data.add(data);
	}
	
	public int getLine(String label)
	{
		for (int i = 0; i < data.size(); i++)
		{
			LineData line = data.get(i);
			if (line.getLabel() != null && line.getLabel().equals(label))
				return i;
		}
		throw new ParsingException("Unknown label \"" + label + "\"");
	}
	
	public String generateJumpLabel() 
	{
		return "_jump_" + generatedJumps++;
	}
	
	public void parse()
	{
		data = parser.parse(this);
	}
	
	public void process()
	{
		boolean proc = true;
		while (proc)
		{
			processLines();
			
			proc = false;
			for (LineData line : data)
			{
				if (!line.isProcessed())
				{
					proc = true;
					break;
				}
			}
		}
	}
	
	private void processLines()
	{
		List<LineData> toProcess = data;
		data = new ArrayList<LineData>();
		
		for (LineData line : toProcess)
		{
			data.add(line);
			if (!line.isProcessed())
				line.process(this);
		}
	}
	
	public List<String> compile()
	{
		List<String> compiled = new ArrayList<String>();
		for (int i = 0; i < data.size(); i++)
		{
			LineData line = data.get(i);
			String compiledLine = line.compile(this, i);
			compiled.add(compiledLine);
		}
		
		return compiled;
	}

}
