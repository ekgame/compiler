package lt.ekgame.compiler.functions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.LineData;
import lt.ekgame.compiler.ParsingException;

public class FunctionReset extends Function {
	
	public static final Map<String, Integer> OFFSETS = new HashMap<>();
	public static final int OFFSET = 64;
	
	static {
		OFFSETS.put("A",    OFFSET);
		OFFSETS.put("B",    OFFSET + 1);
		OFFSETS.put("C",    OFFSET + 2);
		OFFSETS.put("D",    OFFSET + 3);
		OFFSETS.put("E",    OFFSET + 4);
		OFFSETS.put("F",    OFFSET + 5);
		OFFSETS.put("CNT",  OFFSET + 6);
		OFFSETS.put("ROM",  OFFSET + 7);
		OFFSETS.put("FLAG", OFFSET + 8);
	}

	@Override
	public void execute(Context context, LineData line, List<String> arguments) 
	{
		for (String arg : arguments)
		{
			if (!OFFSETS.containsKey(arg))
				throw new ParsingException("Failed to parse RESET function - unknown argument \"" + arg + "\"");
			line.setDataAt(OFFSETS.get(arg), true);
		}
	}

}
