package lt.ekgame.compiler.functions;

import java.util.List;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.LineData;

public class FunctionEmpty extends Function {

	@Override
	public void execute(Context context, LineData line, List<String> arguments) 
	{
		// Does nothing
	}
}
