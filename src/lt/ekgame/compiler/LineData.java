package lt.ekgame.compiler;

import static lt.ekgame.compiler.TokenType.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lt.ekgame.compiler.functions.Function;

public class LineData {
	
	public static final Map<String, Integer> REGISTER_OFFSETS = new HashMap<>();
	
	static {
		REGISTER_OFFSETS.put("A", 8);
		REGISTER_OFFSETS.put("B", 16);
		REGISTER_OFFSETS.put("C", 24);
		REGISTER_OFFSETS.put("D", 32);
		REGISTER_OFFSETS.put("E", 40);
		REGISTER_OFFSETS.put("F", 48);
	}
	
	public static final Map<String, Integer> MUX_OFFSETS = new HashMap<>();
	
	static {
		MUX_OFFSETS.put("in", 1);
		MUX_OFFSETS.put("A", 2);
		MUX_OFFSETS.put("B", 3);
		MUX_OFFSETS.put("C", 4);
		MUX_OFFSETS.put("D", 5);
		MUX_OFFSETS.put("E", 6);
		MUX_OFFSETS.put("F", 7);
	}
	
	public static final Map<String, Integer> CONDITONS = new HashMap<>();
	
	static {
//		CONDITONS.put("A_H",   0);
//		CONDITONS.put("A_L",   1);
//		CONDITONS.put("B_H",   2);
//		CONDITONS.put("B_L",   3);
//		CONDITONS.put("C_H",   4);
//		CONDITONS.put("C_L",   5);
//		CONDITONS.put("D_H",   6);
//		CONDITONS.put("D_L",   7);
//		CONDITONS.put("E_H",   8);
//		CONDITONS.put("E_L",   9);
//		CONDITONS.put("F_H",   10);
//		CONDITONS.put("F_L",   11);
//		CONDITONS.put("CNT",   12);
//		CONDITONS.put("ALU_H", 13);
//		CONDITONS.put("ALU_T", 14); // transfer
//		CONDITONS.put("JUMP",  15);
		
		CONDITONS.put("A_H",   1);
		CONDITONS.put("A_L",   2);
		CONDITONS.put("B_H",   3);
		CONDITONS.put("B_L",   4);
		CONDITONS.put("C_H",   5);
		CONDITONS.put("C_L",   6);
		CONDITONS.put("D_H",   7);
		CONDITONS.put("D_L",   8);
		CONDITONS.put("E_H",   9);
		CONDITONS.put("E_L",   10);
		CONDITONS.put("F_H",   11);
		CONDITONS.put("F_L",   12);
		CONDITONS.put("CNT",   13);
		CONDITONS.put("ALU_H", 14);
		CONDITONS.put("JUMP",  15);
	}
	
	public static final int DATA_SIZE = 75;
	private StringBuilder compiledData = new StringBuilder(new String(new char[DATA_SIZE]).replace("\0", "0"));
	
	private Triplet conditional = Triplet.UNDEFINED;
	private String lineLabel;
	private String selectedMux = null;
	private List<String> writeTo = null;
	private List<Pair<Function, List<String>>> functions = new ArrayList<>();
	
	private String condition = null;
	private String conditionalTarget = null;
	private String conditionalFallback = null;
	
	private boolean processed = false;
	
	public LineData() {}
	
	public LineData(Context context, Tokenizer tokenizer)
	{
		while (tokenizer.hasNext())
		{
			Token token = tokenizer.nextToken(
				LINE_LABEL, ARGUMENT, FUNCTION, CONDITIONAL, COMMENT, ASSIGNER
			);
			
			if (token.getType() == LINE_LABEL)
			{
				if (lineLabel != null)
					throw new ParsingException("Can't assing a second lable. Can't to override \"" + lineLabel + "\" with \"" + token.getValue() + "\"");
				else
					lineLabel = token.getValue();
			}
			
			if (token.getType() == ARGUMENT)
			{
				if (conditional == Triplet.TRUE)
					throw new ParsingException("Invalid operation for a conditional statement.");
				conditional = Triplet.FALSE;
				
				if (selectedMux != null)
					throw new ParsingException("MUX Selector is already defined");
				else
				{
					selectedMux = token.getValue();
					Token nextToken = tokenizer.nextToken(ASSIGNER, STATEMENT_END);
					if (nextToken.getType() == ASSIGNER)
						writeTo = getArguments(tokenizer, STATEMENT_END);
				}
			}
			
			if (token.getType() == ASSIGNER)
			{
				if (conditional == Triplet.TRUE)
					throw new ParsingException("Invalid operation for a conditional statement.");
				conditional = Triplet.FALSE;

				writeTo = getArguments(tokenizer, STATEMENT_END);
			}
			
			if (token.getType() == FUNCTION)
			{
				if (conditional == Triplet.TRUE)
					throw new ParsingException("Invalid operation for a conditional statement.");
				conditional = Triplet.FALSE;
				
				Function function = context.getFunction(token.getValue());
				for (Pair<Function, List<String>> func : functions)
					if (func.getFirst().getClass().equals(function.getClass()) && !function.isCompatible(func.getFirst()))
						throw new ParsingException("Can't apply the function \"" + token.getValue() + "\" twice.");
				
				List<String> arguments = getArguments(tokenizer, BRACKET_CLOSED);
				functions.add(new Pair<Function, List<String>>(function, arguments));
				tokenizer.nextToken(STATEMENT_END);
			}
			
			if (token.getType() == CONDITIONAL)
			{
				if (conditional == Triplet.FALSE)
					throw new ParsingException("Invalid operation for a non-conditional statement.");
				conditional = Triplet.TRUE;
				
				condition = tokenizer.nextToken(TokenType.ARGUMENT).getValue();
				tokenizer.nextToken(TokenType.ASSIGNER);
				Token argument = tokenizer.nextToken(TokenType.ARGUMENT);
				conditionalTarget = argument.getValue();
				
				Token next = tokenizer.nextToken(STATEMENT_END, ELSE);
				if (next.getType() == ELSE)
				{
					Token fallback = tokenizer.nextToken(ARGUMENT);
					conditionalFallback = fallback.getValue();
					tokenizer.nextToken(STATEMENT_END);
				}
			}
		}
	}
	
	public List<String> getArguments(Tokenizer tokenizer, TokenType expectedFinish)
	{
		List<String> arguments = new ArrayList<>();
		while (tokenizer.hasNext())
		{
			Token token = tokenizer.nextToken(ARGUMENT, EXTENDED_ARGUMENT, expectedFinish);
			if (token.getType() == expectedFinish)
				break;
			
			if (token.getType() == ARGUMENT || token.getType() == EXTENDED_ARGUMENT)
				arguments.add(token.getValue());
			
			token = tokenizer.nextToken(SEPERATOR, expectedFinish);
			if (token.getType() == expectedFinish)
				break;
		}
		return arguments;
	}
	
	public String getLabel() {
		return lineLabel;
	}
	
	public boolean getDataAt(int index) {
		return compiledData.charAt(index) == '1';
	}
	
	public void setDataAt(int index, boolean data) {
		compiledData.setCharAt(index, data ? '1' : '0');
	}
	
	public void setDataAt(int index, int data, int length) 
	{
		for (int i = length; i > 0; i--)
		{
			setDataAt(index + i - 1, (data & 1) == 1);
			data = data >> 1;
		}
	}
	
	public boolean isProcessed() {
		return processed;
	}
	
	private LineData createJump(String label)
	{
		LineData line = new LineData();
		line.conditional = Triplet.TRUE;
		line.condition = "JUMP";
		line.conditionalTarget = label;
		line.processed = true;
		return line;
	}
	
	public void process(Context context)
	{
		if (conditional == Triplet.TRUE && !condition.equals("JUMP")) 
		{
			LineData line1 = null;
			LineData line2 = null;
			
			line1 = createJump(conditionalTarget);
			if (conditionalFallback == null)
			{
				String jumpLabel = context.generateJumpLabel();
				conditionalFallback = jumpLabel;
				line2 = new LineData();
				line2.lineLabel = jumpLabel;
				line2.processed = true;
			}
			
			context.addData(line1);
			context.addData(line2);
		}
		processed = true;
	}
	
	public String compile(Context context, int line)
	{
		if (conditional == Triplet.TRUE)
		{
			setDataAt(0, true);
			setDataAt(1, CONDITONS.get(condition), 4);
			if (condition.equals("JUMP"))
				setDataAt(5, context.getLine(conditionalTarget), 8);
			else
				setDataAt(5, context.getLine(conditionalFallback), 8);
		}
		else
		{
			if (selectedMux != null)
				setDataAt(MUX_OFFSETS.get(selectedMux), true);
			
			if (writeTo != null)
			{
				for (String arg : writeTo)
				{
					setDataAt(REGISTER_OFFSETS.get(arg) + 1, true);
				}
			}
			
			for (Pair<Function, List<String>> entry : functions)
				entry.getFirst().execute(context, this, entry.getSecond());
		}
		return compiledData.toString();
	}

}
