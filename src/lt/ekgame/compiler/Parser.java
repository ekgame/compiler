package lt.ekgame.compiler;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Parser {
	
	private Reader input;
	
	public Parser(Reader input)
	{
		this.input = input;
	}
	
	public List<LineData> parse(Context context)
	{
		List<LineData> lineData = new ArrayList<LineData>();
		int line = 1;
		try {
			Scanner scanner = new Scanner(input);
			while (scanner.hasNextLine()) {
				LineData data = parseLine(scanner.nextLine(), context);
				if (data != null)
					lineData.add(data);
				line++;
			}
			scanner.close();
		}
		catch (ParsingException e)
		{
			e.printStackTrace();
			throw new ParsingException("Line " + line + ":" + e.getCharacterIndex() + ": " + e.getMessage());
		}
		return lineData;
	}
	
	private LineData parseLine(String line, Context context)
	{
		line = line.trim();
		if (line.isEmpty() || line.startsWith("#"))
			return null;
		
		Tokenizer tokenizer = new Tokenizer(line);
		return new LineData(context, tokenizer);
	}
}
