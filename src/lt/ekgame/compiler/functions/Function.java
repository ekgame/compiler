package lt.ekgame.compiler.functions;

import java.util.List;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.LineData;

public abstract class Function {
	
	public abstract void execute(Context context, LineData line, List<String> arguments);

	public boolean isCompatible(Function other) {
		return false;
	}
}
