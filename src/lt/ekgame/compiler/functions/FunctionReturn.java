package lt.ekgame.compiler.functions;

import java.util.List;

import lt.ekgame.compiler.Context;
import lt.ekgame.compiler.LineData;
import lt.ekgame.compiler.ParsingException;

public class FunctionReturn extends Function {
	
	public static final int OFFSET = 73;

	@Override
	public void execute(Context context, LineData line, List<String> arguments) 
	{
		if (arguments.size() > 0)
			throw new ParsingException("Failed to parse OUTPUT function - too many arguments");
		
		line.setDataAt(OFFSET, true);
	}

}
