package lt.ekgame.compiler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer {
	
	private String text;
	private String textOriginal;
	private static List<Pair<Pattern, TokenType>> patterns = new ArrayList<>();
	
	static {
		addPattern("if", TokenType.CONDITIONAL);
		addPattern("(?![0-9])[a-zA-Z0-9]+\\s*\\(", TokenType.FUNCTION);
		addPattern("(?![0-9])[a-zA-Z0-9_]+\\:", TokenType.LINE_LABEL);
		addPattern("(?![0-9])[a-zA-Z0-9_]+", TokenType.ARGUMENT);
		addPattern("->", TokenType.ASSIGNER);
		addPattern(";", TokenType.STATEMENT_END);
		addPattern("\\:", TokenType.ELSE);
		addPattern(",", TokenType.SEPERATOR);
		addPattern("\\(", TokenType.BRACKET_OPEN);
		addPattern("\\)", TokenType.BRACKET_CLOSED);
		addPattern("[0-9]+[^a-zA-Z]", TokenType.INTEGER);
		addPattern("#.*", TokenType.COMMENT);
	}
	
	private static void addPattern(String pattern, TokenType type)
	{
		patterns.add(new Pair<Pattern, TokenType>(Pattern.compile("^(" + pattern + ")"), type));
	}
	
	public Tokenizer(String text)
	{
		this.text = text;
		this.textOriginal = text;
	}
	
	public Token nextToken(TokenType... expected)
	{
		Pair<Pattern, TokenType> pattern = null;
		String token = null;
		
		for (Pair<Pattern, TokenType> entry : patterns)
		{
			Matcher matcher = entry.getFirst().matcher(text);
			if (matcher.find())
			{
				pattern = entry;
				token = matcher.group().trim();
				text = matcher.replaceFirst("").trim();
				break;
			}
		}
		
		if (pattern == null)
			throw new ParsingException("Failed to parse \"" + text + "\"");
		
		if (expected != null && expected.length != 0)
		{
			List<TokenType> expectedTypes = Arrays.asList(expected);
			if (!expectedTypes.contains(pattern.getSecond()))
				throw new ParsingException("Unexpected token \"" + token + "\" " + pattern.getSecond().name(), textOriginal.length() - text.length() - 1);
		}
		
		TokenType type = pattern.getSecond();
		if (type == TokenType.LINE_LABEL || type == TokenType.FUNCTION)
			token = token.substring(0, token.length() - 1).trim();
		
		return new Token(token, type);
	}
	
	public boolean hasNext() {
		return !text.trim().isEmpty();
	}

}
